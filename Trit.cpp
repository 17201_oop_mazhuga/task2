//
// Created by 214 on 23.09.2018.
//

#include "Trit.h"

Trit &operator&=(Trit &a, const Trit &b) {
    if (a == Trit::False || b == Trit::False)
        a = Trit::False;
    else if (a == Trit::Unknown || b == Trit::Unknown)
        a = Trit::Unknown;
    else
        a = Trit::True;
    return a;
}

Trit &operator|=(Trit &a, const Trit &b) {
    if (a == Trit::True || b == Trit::True)
        a = Trit::True;
    else if (a == Trit::Unknown || b == Trit::Unknown)
        a = Trit::Unknown;
    else
        a = Trit::False;
    return a;
}

Trit operator&(const Trit &a, const Trit &b) {
    Trit c = a;
    return c &= b;
}

Trit operator|(const Trit &a, const Trit &b) {
    Trit c = a;
    return c |= b;
}

Trit operator!(const Trit &a) {
    Trit c;
    if (a == Trit::True)
        c = Trit::False;
    else if (a == Trit::False)
        c = Trit::True;
    else
        c = Trit(Trit::Unknown);

    return c;
}

std::ostream &operator<<(std::ostream &out, const Trit &t){
    if (t == Trit::True)
        out << '1';
    else if (t == Trit::False)
        out << '0';
    else
        out << '?';
    return out;
}