//
// Created by 214 on 23.09.2018.
//
#include <cstddef>
#include <vector>
#include <string>

#include "Trit.h"

#ifndef INC_02_TRITSET_TRITSET_H
#define INC_02_TRITSET_TRITSET_H

class Tritset {
    typedef unsigned uint;

    //do not change these
    static const uint TRITS_PER_UINT = sizeof(uint) * 8 / 2;
    static const uint MASK = 0b11;

    std::vector <uint> array;

    long long int lastIndex;
    size_t cardinalityTrue;
    size_t cardinalityFalse;
    size_t cardinalityUnknown;

    uint getFillValue(const Trit &tr);

    Trit getValue(const uint index) const;
    void setValue(const uint index, const Trit &other);

public:
    Tritset();
    Tritset(const Tritset &other);
    explicit Tritset(size_t size, Trit fill = Trit::Unknown);
    Tritset(std::initializer_list<Trit>);

    size_t capacity() const;
    size_t length() const;
    size_t cardinality(Trit value) const;
    size_t getTritsPerUint() const;
    std::string toString() const;

    void trim(size_t index);
    void extend(size_t newSize);
    void shrink();
    void push(const Trit &value);

    void update();
    void update(size_t index, Trit prevState);

    class TritProxy {
    private:
        Tritset &parent;
        size_t index;
    public:
        TritProxy(Tritset &tset, size_t index);

        TritProxy &operator=(const Trit &other);
        TritProxy &operator&=(const Trit &other);
        TritProxy &operator|=(const Trit &other);

        operator Trit() const;              //TODO: make it explicit or delete it
    };

    Trit operator[](size_t index) const;    //TODO: const must be preferable
    TritProxy operator[](size_t index);

    Tritset &operator&=(const Tritset &other);
    Tritset &operator|=(const Tritset &other);
    Tritset operator!() const;
    Tritset operator&(const Tritset &other) const;
    Tritset operator|(const Tritset &other) const;

    bool operator==(const Tritset &other) const;
    bool operator!=(const Tritset &other) const;

    friend std::ostream& operator<<(std::ostream &out, const Tritset &tset);

    class Iterator : public std::iterator<Trit, Tritset>{
    private:
        Tritset &parent;
        uint index;
    public:
        Iterator(Tritset &tset, uint index);
        Iterator& operator++();
        bool operator==(const Iterator &other);
        bool operator!=(const Iterator &other);
        Trit operator*();
    };

    Iterator begin();
    Iterator end();
};


#endif //INC_02_TRITSET_TRITSET_H
