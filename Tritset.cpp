//
// Created by 214 on 23.09.2018.
//

#include "Tritset.h"

Tritset::Tritset() : lastIndex(-1), cardinalityTrue(0), cardinalityFalse(0), cardinalityUnknown(0) {
}

Tritset::Tritset(const size_t size, const Trit fill) : cardinalityTrue(0), cardinalityFalse(0), cardinalityUnknown(0) {
    lastIndex = size - 1;

    size_t neededSize = size / TRITS_PER_UINT;
    if (size % TRITS_PER_UINT != 0)
        neededSize++;

    array.resize(neededSize, getFillValue(fill));
    if (size % TRITS_PER_UINT != 0 && fill != Trit::Unknown)
        for (int i = size; i < neededSize * TRITS_PER_UINT; i++)
            (*this)[i] = Trit::Unknown;

    switch (fill) {
        case Trit::True:
            cardinalityTrue = size;
            break;
        case Trit::False:
            cardinalityFalse = size;
            break;
        case Trit::Unknown:
            cardinalityUnknown = size;
            break;
    }
}

Tritset::Tritset(const Tritset &other) : array(other.array), lastIndex(other.lastIndex),
                                         cardinalityTrue(other.cardinalityTrue),
                                         cardinalityFalse(other.cardinalityFalse),
                                         cardinalityUnknown(other.cardinalityUnknown) {
}

Tritset::Tritset(std::initializer_list<Trit> list) : lastIndex(-1), cardinalityTrue(0), cardinalityFalse(0), cardinalityUnknown(0){
    for(Trit t : list)
        push(t);
}

size_t Tritset::capacity() const {
    return array.size() * TRITS_PER_UINT;
}

size_t Tritset::length() const {
    return static_cast<size_t>(lastIndex + 1);
}

void Tritset::trim(const size_t index) {
    size_t neededSize = index / TRITS_PER_UINT;
    if (index % TRITS_PER_UINT != 0)
        neededSize++;

    array.resize(neededSize);
    for (int i = index; i < neededSize * TRITS_PER_UINT; i++)
        (*this)[i] = Trit::Unknown;
}

void Tritset::shrink() {
    if (lastIndex < 0)
        array.resize(0);
    else
        trim(static_cast<size_t>(lastIndex));
}

void Tritset::update() {
    long long int last = -1;
    size_t cardT = 0,
            cardF = 0,
            cardU = 0,
            cardU_temp = 0;

    //need it because we do not know if unknown trits we count are before or after last

    size_t cap = capacity();
    Trit current;

    for (size_t i = 0; i < cap; i++) {
        current = (*this)[i];
        if (current == Trit::Unknown)
            cardU_temp++;
        else if (current == Trit::True) {
            cardT++;
            cardU += cardU_temp;
            cardU_temp = 0;

            last = i;
        } else if (current == Trit::False) {
            cardF++;
            cardU += cardU_temp;
            cardU_temp = 0;

            last = i;
        }
    }

    lastIndex = last;
    cardinalityTrue = cardT;
    cardinalityFalse = cardF;
    cardinalityUnknown = cardU;
}

void Tritset::update(const size_t index, const Trit prevState) {
    Trit curState = (*this)[index];

    if (curState == prevState)
        return;
    if (index > lastIndex && curState == Trit::Unknown) {
        return;
    }
    if (index == lastIndex && curState == Trit::Unknown) {
        update();
        return;
    }

    if (curState == Trit::Unknown)
        cardinalityUnknown++;
    else if (curState == Trit::True) {
        cardinalityTrue++;
    } else if (curState == Trit::False) {
        cardinalityFalse++;
    }

    if (index > lastIndex) {
        lastIndex = index;
        return;
    }

    if (prevState == Trit::Unknown)
        cardinalityUnknown--;
    else if (prevState == Trit::True) {
        cardinalityTrue--;
    } else if (prevState == Trit::False) {
        cardinalityFalse--;
    }
}

void Tritset::extend(const size_t newSize) {
    size_t neededSize = newSize / TRITS_PER_UINT;
    if (newSize % TRITS_PER_UINT != 0)
        neededSize++;

    if (neededSize > array.size())
        array.resize(neededSize, getFillValue(Trit::Unknown));
}

Tritset::uint Tritset::getFillValue(const Trit &tr) {
    uint temp, fill = 0;
    temp = static_cast<uint>(tr);
    for (int i = 0; i < TRITS_PER_UINT; i++)
        fill |= temp << (i * 2);
    return fill;
}

Trit Tritset::operator[](const size_t index) const {
    return getValue(index);
}

Tritset::TritProxy Tritset::operator[](const size_t index) {
    return {*this, index};
}

std::string Tritset::toString() const {
    std::string s;
    size_t cap = capacity();
    Trit currentTrit;

    for (int i = 0; i < capacity(); i++) {
        currentTrit = (*this)[i];
        if (currentTrit == Trit::True)
            s += '1';
        else if (currentTrit == Trit::False)
            s += '0';
        else
            s += '?';
    }

    return s;
}

Tritset &Tritset::operator&=(const Tritset &other) {
    size_t newLength = other.length();
    extend(newLength);

    for (size_t i = 0; i < newLength; i++)
        (*this)[i] &= other[i];
    for (size_t i = newLength; i <= lastIndex; i++)
        (*this)[i] &= Trit::Unknown;
    update();
    return (*this);
}

Tritset &Tritset::operator|=(const Tritset &other) {
    size_t newLength = other.length();
    extend(newLength);

    for (size_t i = 0; i < newLength; i++)
        (*this)[i] |= other[i];
    for (size_t i = newLength; i <= lastIndex; i++)
        (*this)[i] |= Trit::Unknown;
    update();
    return *this;
}

Tritset Tritset::operator!() const {
    Tritset t(this->length());
    for (size_t i = 0; i <= lastIndex; i++)
        t[i] = !(*this)[i];
    return t;
}

Tritset Tritset::operator&(const Tritset &other) const {
    Tritset t = (*this);
    t &= other;
    return t;
}

Tritset Tritset::operator|(const Tritset &other) const {
    Tritset t = (*this);
    t |= other;
    return t;
}

size_t Tritset::cardinality(Trit value) const {
    if (value == Trit::True)
        return cardinalityTrue;
    if (value == Trit::False)
        return cardinalityFalse;
    return cardinalityUnknown;
}

bool Tritset::operator==(const Tritset &other) const {
    if (lastIndex != other.lastIndex ||
          cardinalityTrue != other.cardinalityTrue ||
          cardinalityFalse != other.cardinalityFalse ||
          cardinalityUnknown != other.cardinalityUnknown ||
          length() != other.length())
        return false;

    return array == other.array;
}

bool Tritset::operator!=(const Tritset &other) const {
    return !((*this) == other);
}

size_t Tritset::getTritsPerUint() const {
    return TRITS_PER_UINT;
}

void Tritset::push(const Trit &value) {
    extend(length() + 1);
    (*this)[++lastIndex] = value;
}

Trit Tritset::getValue(const Tritset::uint index) const{
    Trit t;
    if (index <= capacity() * TRITS_PER_UINT) {
        uint num = array[index / TRITS_PER_UINT];
        num >>= 2 * (index % TRITS_PER_UINT);
        num &= MASK;
        t = static_cast<Trit>(num);
    } else
        t = Trit::Unknown;

    return t;
}

void Tritset::setValue(const Tritset::uint index, const Trit &other) {
    if (index > capacity()) {
        if (other == Trit::Unknown)
            return;
        else
            extend(index + 1);
    }

    Trit prev = (*this)[index];

    uint num = array[index / TRITS_PER_UINT];
    uint trit = static_cast<uint>(other);

    trit <<= 2 * (index % TRITS_PER_UINT);
    num &= ~(MASK << 2 * (index % TRITS_PER_UINT));
    num |= trit;

    array[index / TRITS_PER_UINT] = num;

    update(index, prev);
}

Tritset::Iterator Tritset::begin() {
    return Tritset::Iterator(*(this), 0);
}

Tritset::Iterator Tritset::end() {
    return Tritset::Iterator(*(this), length());
}

std::ostream &operator<<(std::ostream &out, const Tritset &tset) {
    out << tset.toString();
    return out;
}

Tritset::TritProxy::TritProxy(Tritset &tset, size_t index) : parent(tset), index(index) {
}

Tritset::TritProxy &Tritset::TritProxy::operator=(const Trit &other) {
    parent.setValue(index, other);
    return (*this);
}

Tritset::TritProxy &Tritset::TritProxy::operator&=(const Trit &other) {
    parent.setValue(index, parent.getValue(index) & other);
    return *this;
}

Tritset::TritProxy &Tritset::TritProxy::operator|=(const Trit &other) {
    parent.setValue(index, parent.getValue(index) | other);
    return *this;
}

Tritset::TritProxy::operator Trit() const {
    return parent.getValue(index);
}

Tritset::Iterator::Iterator(Tritset &tset, Tritset::uint index) : parent(tset), index(index){
}

Tritset::Iterator &Tritset::Iterator::operator++() {
    index++;
    return(*this);
}

bool Tritset::Iterator::operator==(const Tritset::Iterator &other) {
    return index == other.index && parent == other.parent;
}

bool Tritset::Iterator::operator!=(const Tritset::Iterator &other) {
    return !(*this == other);
}

Trit Tritset::Iterator::operator*() {
    return parent.getValue(index);
}
