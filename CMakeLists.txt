cmake_minimum_required(VERSION 3.7)
project(02_tritset)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES Tritset.cpp Tritset.h Trit.cpp Trit.h)

add_executable(02_tritset main.cpp ${SOURCE_FILES})
add_executable(unittest test.cpp ${SOURCE_FILES})