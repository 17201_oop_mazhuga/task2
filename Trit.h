//
// Created by 214 on 23.09.2018.
//

#ifndef INC_02_TRITSET_TRIT_H
#define INC_02_TRITSET_TRIT_H

#include <iostream>

enum class Trit {         //you can change it
    False = 0,
    True = 1,
    Unknown = 2
};

Trit &operator&=(Trit &a, const Trit &b);
Trit &operator|=(Trit &a, const Trit &b);

Trit operator&(const Trit &a, const Trit &b);
Trit operator|(const Trit &a, const Trit &b);
Trit operator!(const Trit &a);

std::ostream& operator <<(std::ostream &out, const Trit &t);
//TODO: char conversion?

#endif //INC_02_TRITSET_TRIT_H