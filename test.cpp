//
// Created by 214 on 30.09.2018.
//

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file

#include "catch.hpp"

#include "Trit.h"
#include "Tritset.h"

TEST_CASE("Test: Trit Logic Operations") {
    Trit t = Trit::True;
    Trit f = Trit::False;
    Trit u = Trit::Unknown;

    REQUIRE((t & t) == Trit::True);
    REQUIRE((t & f) == Trit::False);
    REQUIRE((t & u) == Trit::Unknown);
    REQUIRE((f & f) == Trit::False);
    REQUIRE((f & u) == Trit::False);
    REQUIRE((u & u) == Trit::Unknown);

    REQUIRE((t | t) == Trit::True);
    REQUIRE((t | f) == Trit::True);
    REQUIRE((t | u) == Trit::True);
    REQUIRE((f | f) == Trit::False);
    REQUIRE((f | u) == Trit::Unknown);
    REQUIRE((u | u) == Trit::Unknown);

    REQUIRE(!t == Trit::False);
    REQUIRE(!f == Trit::True);
    REQUIRE(!u == Trit::Unknown);
}

TEST_CASE("Test: Trit Stream Output"){
    Trit t = Trit::True;
    Trit f = Trit::False;
    Trit u = Trit::Unknown;

    std::stringstream out;

    out << t << f << u;

    REQUIRE(out.str() == "10?");
}

TEST_CASE("Test: Tritset Constructors", "tritset") {
    Tritset a, b(10, Trit::True), c = b;

    REQUIRE(a.length() == 0);
    REQUIRE(b.length() == 10);
    REQUIRE(b[5] == Trit::True);
    REQUIRE(b.cardinality(Trit::True) == 10);

    REQUIRE(c == b);
}

TEST_CASE("Test: Tritset Length & Capacity", "tritset") {
    size_t size = 26;
    Tritset a(size, Trit::False);

    size_t cap = size / a.getTritsPerUint();
    if (size % a.getTritsPerUint())
        cap++;
    cap *= a.getTritsPerUint();

    REQUIRE(a.length() == size);
    REQUIRE(a.capacity() == cap);
}

TEST_CASE("Test: Tritset Cardinality", "tritset") {
    Tritset a(15, Trit::True);

    REQUIRE(a.cardinality(Trit::True) == 15);
    REQUIRE(a.cardinality(Trit::False) == 0);
    REQUIRE(a.cardinality(Trit::Unknown) == 0);

    a[0] = Trit::False;
    a[3] = Trit::False;
    a[5] = Trit::Unknown;

    REQUIRE(a.cardinality(Trit::True) == 12);
    REQUIRE(a.cardinality(Trit::False) == 2);
    REQUIRE(a.cardinality(Trit::Unknown) == 1);
}

TEST_CASE("Test: Tritset Changing Size", "tritset") {
    Tritset a(16, Trit::True);
    a.trim(14);
    REQUIRE(a.length() == 14);

    a[13] = Trit::Unknown;
    REQUIRE(a.length() == 13);

    a.extend(36);
    REQUIRE(a.length() == 13);

    a[34] = Trit::False;
    REQUIRE(a.length() == 35);

    a[500] = Trit::True;
    REQUIRE(a.length() == 501);
}

TEST_CASE("Test: Tritset Stream Output", "tritset") {
    Tritset a(3);
    a[0] = Trit::True;
    a[1] = Trit::Unknown;
    a[2] = Trit::False;

    std::string s = "1?0";
    for (int i = 0; i < (a.getTritsPerUint() - 3); i++)
        s += "?";

    REQUIRE(a.toString() == s);

    std::stringstream out;
    out << a;

    REQUIRE(out.str() == s);
}

TEST_CASE("Test: Tritset Tritproxy Logic Operations", "tritset") {
    Tritset a(4);
    a[1] = Trit::True;
    a[2] = Trit::False;
    a[3] = Trit::Unknown;

    REQUIRE((a[1] & a[1]) == Trit::True);
    REQUIRE((a[1] & a[2]) == Trit::False);
    REQUIRE((a[1] & a[3]) == Trit::Unknown);
    REQUIRE((a[2] & a[2]) == Trit::False);
    REQUIRE((a[2] & a[3]) == Trit::False);
    REQUIRE((a[3] & a[3]) == Trit::Unknown);

    REQUIRE((a[1] | a[1]) == Trit::True);
    REQUIRE((a[1] | a[2]) == Trit::True);
    REQUIRE((a[1] | a[3]) == Trit::True);
    REQUIRE((a[2] | a[2]) == Trit::False);
    REQUIRE((a[2] | a[3]) == Trit::Unknown);
    REQUIRE((a[3] | a[3]) == Trit::Unknown);

    REQUIRE(!a[1] == Trit::False);
    REQUIRE(!a[2] == Trit::True);
    REQUIRE(!a[3] == Trit::Unknown);
}

TEST_CASE("Test: Tritset Logic Operations", "tritset") {
    Tritset a(3);
    Tritset b(4);
    Tritset c(1);
    std::string s;

    a[0] = Trit::True;
    a[1] = Trit::False;
    a[2] = Trit::Unknown;

    b[0] = Trit::Unknown;
    b[1] = Trit::True;
    b[2] = Trit::False;
    b[3] = Trit::Unknown;

    c = a & b;
    s = "?00?";
    for (int i = 0; i < (a.getTritsPerUint() - 4); i++)
        s += "?";
    REQUIRE(c.toString() == s);

    c = a | b;
    s = "11??";
    for (int i = 0; i < (a.getTritsPerUint() - 4); i++)
        s += "?";
    REQUIRE(c.toString() == s);

    c = !a;
    s = "01?";
    for (int i = 0; i < (a.getTritsPerUint() - 3); i++)
        s += "?";
    REQUIRE(c.toString() == s);
}

TEST_CASE("Test: Tritset Comparison", "tritset") {
    Tritset a(4), b(4);
    REQUIRE(a == b);

    a[3] = Trit::True;
    REQUIRE(a != b);

    b[3] = Trit::False;
    REQUIRE(a != b);

    b = a;
    REQUIRE(a == b);
}

TEST_CASE("Test: Tritset Initializer List", "tritset"){
    Tritset t1{Trit::True, Trit::False, Trit::Unknown, Trit::True};
    Tritset t2{Trit::True, Trit::True};

    std::string s1 = "10?1";
    for (int i = 0; i < (t1.getTritsPerUint() - 4); i++)
        s1 += "?";

    std::string s2 = "11";
    for (int i = 0; i < (t2.getTritsPerUint() - 2); i++)
        s2 += "?";

    REQUIRE(t1.toString() == s1);
    REQUIRE(t2.toString() == s2);
}

TEST_CASE("Test: Tritset Iterators", "tritset"){
    Tritset ts{Trit::True, Trit::False, Trit::Unknown, Trit::True};
    std::stringstream out1, out2;

    out1 << *(ts.begin()) << *(ts.end());

    REQUIRE(out1.str() == "1?");

    for (auto i : ts)
        out2 << i;

    REQUIRE(out2.str() == "10?1");
}

TEST_CASE("Full Test",
          "from https://docs.google.com/document/d/1NM_qbZ3wJUjY_5fa807d3zlcjxzt6d0dX6MIh6Ma5eg/edit") {

    Tritset set(1000);
    size_t allocLength = set.capacity();
    REQUIRE(allocLength >= 1000 * 2 / 8 / sizeof(unsigned));

    set[1000000000] = Trit::Unknown;
    REQUIRE(allocLength == set.capacity());

    if (set[2000000000] == Trit::True) {}
    REQUIRE(allocLength == set.capacity());

    set[1000000000] = Trit::True;
    REQUIRE(allocLength < set.capacity());

    allocLength = set.capacity();
    set[1000000000] = Trit::Unknown;
    set[1000000] = Trit::False;
    REQUIRE(allocLength == set.capacity());

    set.shrink();
    REQUIRE(allocLength > set.capacity());

    Tritset setA(1000);
    Tritset setB(2000);
    Tritset setC = setA & setB;
    REQUIRE(setC.capacity() == setB.capacity());

}